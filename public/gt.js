export function membersMap(root_url, mapid) {
    $.getJSON( root_url + 'members.json',
	       (data) => setupMap(mapid, data));
}


export function printMembers(root_url, containerid) {
    $.getJSON( root_url + 'members.json',
	       (data) => groupMembersThen(data, containerid, printMemberCategory));
}


export function listMembers(root_url, containerid) {
    $.getJSON( root_url + 'members.json',
	       (data) => groupMembersThen(data, containerid, listMemberCategory, listMemberMenu));
}


function renderMemberPopup (member) {
    var address = [member.address, member.address_additional].filter(i => i !== '').join('<br/>');
    return `<h3><a target="_blank" href="${member.website}">${member.name}</a><br/>${member.category}</h3>
                      <p>
                        ${member.description}
                       </p>
                      <address>
                         <p>
                           ${address}<br/>
                           ${member.postcode}&nbsp;${member.city}
                          </p>
                         <a target="_blank" href="${member.website}">Accéder au site internet</a><br/>
                         <a href="tel:+${member.phone_numbers[0]}">${member.phone_numbers[0]}</a><br/>
                         <a href="mailto:${member.mail}">${member.mail}</a>
                      </address>`;
}


function setupMap(mapid, data) {
    L.layerGroup();
    var groups =  new Map();
    var all_markers = [];

    data.forEach( member => {
        if (!groups.has(member.category))
            groups.set(member.category, L.layerGroup());

        if (!member.lat) {
	    console.log(`${member.name} has no coordinates`);
            return;
	}
        
        all_markers.push(L
                         .marker([member.lat, member.lon], { title: `${member.name} (${member.description})` })
                         .bindPopup(function() {return renderMemberPopup(member);})
                         .addTo(groups.get(member.category)));
    });
    

    var osm_url = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
    var base_layer  = L.tileLayer(osm_url,
                                  {attribution: 'OpenStreetMap'});

    var layers = Array.from(groups.values());
    layers.unshift(base_layer);
    
    var map = L
        .map(mapid, { layers: layers })
        .fitWorld();

    var bounds = new L.featureGroup(all_markers);
    bounds.addTo(map);
    map.fitBounds(bounds.getBounds(), {padding: [50, 50]});

    L.control
	.layers({},
		Object.fromEntries(groups),
		{
		    collapsed: false,
		    sortLayers: true
		})
	.addTo(map);

    map
        .on('locationfound',
            e => {
                var radius = e.accuracy / 2;
                L.circle(e.latlng, radius).addTo(map);
            })
        .locate({setView: true, maxZoom: 16});
}


function printMemberCategory(containerid, members, category) {
    var renderMember = function(member) {
	var contact_infos = [member.phone_numbers[0], member.website, member.mail].filter(i => i !== undefined)
	var address = [member.address, member.address_additional].filter(i => i !== '').join('<br/>');
	return `<article>
                        <h3>${member.name}</h3>
                        <p>${member.description}</p>
                        <address>
                           ${address}<br/>
                           ${member.postcode}&nbsp;${member.city}<br/><br/>
                           ${contact_infos.join('<br/>')}
                        </address>
                      </article>`;
    }

    var renderMembers = function(members) {
	return members.sort((a,b) => a.name.localeCompare(b.name)).map(renderMember).join("\n");
    }

    $('body').append(`<section>
                        <header><h2>${category}</h2></header>
                        <div>${renderMembers(members)}</div>
                      </section>`);
}


function listMemberCategory(containerid, members, category) {
    var renderMember = (member) => {
	var phones = member.phone_numbers
	    .map(p => '<a href="tel:' + p + '">' + p + '</a>')
	    .join(' - ');
	
	if (phones)
	    phones += '<br/>';
	
	var address = [member.address, member.address_additional].filter(i => i !== '').join('<br/>');
	return `<h3>${member.name}</h3>
                <span>${member.description}</span>
                <address>
                  ${address}<br/>
                  ${member.postcode}&nbsp;${member.city}<br/>
                  ${phones}
                  <a target="_blank" href="${member.website}">${member.website}</a><br/>
                  <a href="mailto:${member.mail}">${member.mail}</a>
                </address>`;
    }

    
    var renderMembers = (members) => {
	return members.sort((a,b) => a.name.localeCompare(b.name)).map(renderMember).join("\n");
    }
    

    $('#' + containerid)
	.append(`<section>
                    <header><h2 id="${escape(category)}">${category}</h2></header>
                    <div>${renderMembers(members)}</div>
                 </section>`);
}


function listMemberMenu(containerid, categories) {
    var renderLinks = (categories) => {
	return categories.map( (cat) => `<a href="#${escape(cat)}">${cat}</a>`).join("\n")
    }
    
    $('#' + containerid).append(`<div id="member_categories_links">${renderLinks(categories)}</div>`);
}


function groupMembersThen(data, containerid, renderer, menu_renderer) {
    var categories =  new Map();
    
    data.forEach( member => {
        if (!categories.has(member.category))
	    categories.set(member.category, []);
	categories.get(member.category).push(member);
    });

    var catkeys = Array.from(categories.keys()).sort();
    if (menu_renderer)
	menu_renderer(containerid, catkeys);
    catkeys.forEach( key => renderer(containerid, categories.get(key), key) );
}
