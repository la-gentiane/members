<?php
namespace GT\Members;
use GuzzleHttp\Client;

class GoogleDoc {
    public function members($gid) {
        $client = new Client([
            'base_uri' => 'https://docs.google.com',
            'timeout'  => 2.0,
        ]);
        $response = $client->get('spreadsheets/d/' . $gid . '/export',
                                 ['query' => ['format' => 'csv']]);
        return $this->_parseCSV($response->getBody());
    }


    protected function _parseCSV($str) {
        $lines = explode("\n", $str);
        $header = array_map('strtolower', str_getcsv(array_shift($lines)));

        $members = new Members();
        
        array_walk(
            $lines,
            function($line) use ($members, $header)
            {
                $values = array_replace(array_fill_keys(range(0,
                                                              count($header)-1),
                                                        ''),
                                      str_getcsv($line));
                $members->add(
                    $this->_buildMember(
                        array_combine($header,
                                      $values)));
            });
        return $members;
    }


    protected function _buildMember($values) {
        $values = array_merge(
            ['entreprise' => '',
             'activité' => '',
             'catégorie' => '',
             'adresse' => '',
             'complément d\'adresse' => '',
             'cp' => '',
             'commune' => '',
             'site web' => '',
             'téléphone 1' => '',
             'téléphone 2' => ''],
            $values);
        
        return new Member([
            'name' => $values['entreprise'],
            'description' => $values['activité'],
            'category' => $values['catégorie'],
            'address' => $values['adresse'],
            'address_additional' => $values['complément d\'adresse'],
            'postcode' => $values['cp'],
            'city' => $values['commune'],
            'website' => $values['site web'],
            'phone_numbers' => array_values(array_filter([trim($values['téléphone 1']),
                                                          trim($values['téléphone 2'])])),
            'mail' => $values['mail']
        ]);
    }
}
