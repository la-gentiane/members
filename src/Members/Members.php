<?php
namespace GT\Members;

class Members {
    protected $_members = [];

    public static function loadFrom($path) {
        $datas = json_decode(file_get_contents($path), true);
        $members = new static();
        array_walk($datas,
                   function ($attribs) use ($members) {
                       $members->add(new Member($attribs));
                   });

        return $members;
    }


    public function updateCoordinates() {
        array_walk($this->_members,
                   function($m)
                   {
                       $m->updateCoordinates();
                   });
        return $this;
    }

    
    public function add($member) {
        $this->_members []= $member;
        return $this;
    }


    public function saveTo($path) {
        file_put_contents($path, $this->toJSONString());
        return $this;
    }


    public function toJSONString() {
        return json_encode($this->toArray(),
                           JSON_PRETTY_PRINT);
    }


    public function toArray() {
        return array_values(
            array_filter(
                array_map(
                    function($m) {
                        return $m->toArray();
                    },
                    $this->_members),
                function($m) { return !empty($m['name']); })
        );
    }

    
    public function __toString() {
        return $this->toJSONString();
    }
}
