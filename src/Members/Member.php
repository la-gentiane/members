<?php
namespace GT\Members;
use GuzzleHttp\Client;

class Member {
    protected $_attribs;
    
    public function __construct($attribs) {
        $this->_attribs = $attribs;
    }


    public function updateCoordinates() {
        $client = new Client([
            'base_uri' => 'https://nominatim.openstreetmap.org',
            'timeout'  => 2.0,
        ]);

        $response = $client->get('search',
                                 [
                                     'query' => [
                                         'q' => sprintf('%s, %s %s, france',
                                                        $this->_attribs['address'],
                                                        $this->_attribs['city'],
                                                        $this->_attribs['postcode']),
                                         'format' => 'json',
                                         'limit' => '1'
                                     ]
                                 ]);
        if (!$datas = json_decode($response->getBody(), true))
            return $this;

        $this->_attribs['lat'] = $datas[0]['lat'];
        $this->_attribs['lon'] = $datas[0]['lon'];
        return $this;
    }

    public function toArray() {
        return $this->_attribs;
    }


    public function __toString() {
        return json_encode($this->toArray(),
                           JSON_PRETTY_PRINT);
    }
}
